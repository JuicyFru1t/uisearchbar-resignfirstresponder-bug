//
//  SeachBarViewController.swift
//  UISearchBarTest
//
//  Created by Alexander Khodko on 02/03/2018.
//  Copyright © 2018 Alexander Khodko. All rights reserved.
//

import UIKit

class SeachBarViewController: UIViewController {

    @IBOutlet var searchBar: UISearchBar!

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        attemptToHidKeyboard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        attemptToHidKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        attemptToHidKeyboard()
    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            attemptToHidKeyboard()
        }
    }
    
    override func willMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            attemptToHidKeyboard()
        }
    }
    
    
    private func attemptToHidKeyboard() {
        self.searchBar.resignFirstResponder()
        self.searchBar.endEditing(true)
        self.view.resignFirstResponder()
        self.view.endEditing(true)
    }
}
