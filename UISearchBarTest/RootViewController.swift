//
//  ViewController.swift
//  UISearchBarTest
//
//  Created by Alexander Khodko on 02/03/2018.
//  Copyright © 2018 Alexander Khodko. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        searchBarViewController = storyboard?.instantiateViewController(withIdentifier: "SeachBarViewController") as? SeachBarViewController
    }
    
    private var searchBarViewController: SeachBarViewController?

    @IBAction func push(_ sender: UIButton) {
        if let destination = searchBarViewController {
            navigationController?.pushViewController(destination, animated: true)
        }
    }
}

